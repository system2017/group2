package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import ConnectionManager.MyConnectionManager;
import dto.EmpDTO;

public class EmpDAO {

	public int insert(EmpDTO empDTO) {

		MyConnectionManager connectionManager = new MyConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("INSERT INTO public.emp(empid, empname, mail, birthday, salary, password, deptid)");
			sb.append(" VALUES(?, ?, ?, ?, ?, ?, ?)");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());

			// ?にデータをセット
			prepareStatement.setString(1, empDTO.getEmpid());
			prepareStatement.setString(2, empDTO.getName());
			prepareStatement.setString(3, empDTO.getAddress());
			prepareStatement.setDate(4, empDTO.getBirthday());
			prepareStatement.setInt(5, empDTO.getSalary());
			prepareStatement.setString(6, empDTO.getPassword());
			prepareStatement.setString(7, empDTO.getDepartment());

			int result = prepareStatement.executeUpdate();
			connectionManager.commitTransaction();

			return result;
		} catch (SQLException e) {
			connectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}
	}

	public ArrayList<EmpDTO> getAll() {

		MyConnectionManager connectionManager = new MyConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("        emp");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());

			ResultSet rs = prepareStatement.executeQuery();

			ArrayList<EmpDTO> empList = new ArrayList<>();
			while (rs.next()) {
				String c1 = rs.getString("empid");
				String c2 = rs.getString("empname");
				String c3 = rs.getString("mail");
				Date c4 = rs.getDate("birthday");
				int c5 = rs.getInt("salary");
				String c6 = rs.getString("password");
				String c7 = rs.getString("deptid");

				EmpDTO empDTO = new EmpDTO();

				empDTO.setEmpid(c1);
				empDTO.setName(c2);
				empDTO.setAddress(c3);
				empDTO.setBirthday(c4);
				empDTO.setSalary(c5);
				empDTO.setPassword(c6);
				empDTO.setDepartment(c7);

				empList.add(empDTO);

			}

			return empList;
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}
	}

	public EmpDTO getOne(String empid) {

		MyConnectionManager connectionManager = new MyConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("       emp");
			sb.append(" WHERE");
			sb.append(" empid = ?;");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());
			prepareStatement.setString(1, empid);

			ResultSet rs = prepareStatement.executeQuery();

			EmpDTO empOne = null;

			while (rs.next()) {
				String c1 = rs.getString("empid");
				String c2 = rs.getString("empname");
				String c3 = rs.getString("mail");
				Date c5 = rs.getDate("birthday");
				int c6 = rs.getInt("salary");
				String c7 = rs.getString("password");
				String c8 = rs.getString("deptid");

				empOne = new EmpDTO();
				empOne.setEmpid(c1);
				empOne.setName(c2);
				empOne.setAddress(c3);
				empOne.setBirthday(c5);
				empOne.setSalary(c6);
				empOne.setPassword(c7);
				empOne.setDepartment(c8);

			}
			return empOne;
		} catch (SQLException e) {
			throw new RuntimeException(e);
			// TODO 自動生成された catch ブロック
		} finally {
			connectionManager.closeConnection();
		}

	}

	public int delete(String empid) {
		MyConnectionManager connectionManager = new MyConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("delete from emp where empid = ?");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());

			// ?にデータをセット
			prepareStatement.setString(1, empid);

			int result = prepareStatement.executeUpdate();
			connectionManager.commitTransaction();

			return result;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public int update(EmpDTO empDTO) {
		MyConnectionManager connectionManager = new MyConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append(
					"update emp set empname = ?, mail = ?, birthday = ?, salary = ?, password = ?, deptid = ? where empid = ?");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());

			// ?にデータをセット

			prepareStatement.setString(1, empDTO.getName());
			prepareStatement.setString(2, empDTO.getAddress());
			prepareStatement.setDate(3, empDTO.getBirthday());
			prepareStatement.setInt(4, empDTO.getSalary());
			prepareStatement.setString(5, empDTO.getPassword());
			prepareStatement.setString(6, empDTO.getDepartment());
			prepareStatement.setString(7, empDTO.getEmpid());

			int result = prepareStatement.executeUpdate();
			connectionManager.commitTransaction();

			return result;
		} catch (SQLException e) {
			connectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}
	}

	public ArrayList<EmpDTO> search(int setsearch, String elements) {

		MyConnectionManager connectionManager = new MyConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			if (setsearch == 0) {
				sb.append("SELECT");
				sb.append("        *");
				sb.append("    FROM");
				sb.append("        emp");
				sb.append("    WHERE");
				sb.append("        empname LIKE ?");
			} else if (setsearch == 1) {
				sb.append("SELECT");
				sb.append("        *");
				sb.append("    FROM");
				sb.append("        emp");
				sb.append("    WHERE");
				sb.append("        empid LIKE ?");

			}
			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());

			prepareStatement.setString(1, "%" + elements + "%");

			ResultSet rs = prepareStatement.executeQuery();

			ArrayList<EmpDTO> empList = new ArrayList<>();
			while (rs.next()) {
				String c1 = rs.getString("empid");
				String c2 = rs.getString("empname");
				String c3 = rs.getString("mail");
				Date c4 = rs.getDate("birthday");
				int c5 = rs.getInt("salary");
				String c6 = rs.getString("password");
				String c7 = rs.getString("deptid");

				EmpDTO empDTO = new EmpDTO();

				empDTO.setEmpid(c1);
				empDTO.setName(c2);
				empDTO.setAddress(c3);
				empDTO.setBirthday(c4);
				empDTO.setSalary(c5);
				empDTO.setPassword(c6);
				empDTO.setDepartment(c7);

				empList.add(empDTO);

			}
			return empList;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}
	}
}
