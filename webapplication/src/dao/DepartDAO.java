package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import ConnectionManager.MyConnectionManager;
import dto.DepartDTO;

public class DepartDAO {

	public ArrayList<DepartDTO> getAll() {
		MyConnectionManager connectionManager = new MyConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("        dept");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());

			ResultSet rs = prepareStatement.executeQuery();

			ArrayList<DepartDTO> departlist = new ArrayList<>();
			while (rs.next()) {
				String d1 = rs.getString("deptid");
				String d2 = rs.getString("deptname");

				DepartDTO depart = new DepartDTO();
				depart.setDeptid(d1);
				depart.setDeptname(d2);

				departlist.add(depart);
			}
			return departlist;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public DepartDTO getOne(String departid) {

		MyConnectionManager connectionManager = new MyConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("       dept");
			sb.append(" WHERE");
			sb.append(" deptid = ?;");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());
			prepareStatement.setString(1, departid);

			ResultSet rs = prepareStatement.executeQuery();

			DepartDTO departOne = null;

			while (rs.next()) {
				String c1 = rs.getString("deptid");
				String c2 = rs.getString("deptname");

				System.out.println(c2);

				departOne = new DepartDTO();
				departOne.setDeptid(c1);
				departOne.setDeptname(c2);

			}
			return departOne;
		} catch (SQLException e) {
			throw new RuntimeException(e);
			// TODO 自動生成された catch ブロック
		} finally {
			connectionManager.closeConnection();
		}

	}

	public int insert(DepartDTO depart) {
		MyConnectionManager connectionManager = new MyConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("INSERT");
			sb.append("    INTO");
			sb.append("        dept(");
			sb.append("            deptid");
			sb.append("            ,deptname");
			sb.append("        )");
			sb.append("    VALUES");
			sb.append("        (");
			sb.append("            ?");
			sb.append("            ,?");
			sb.append("        );");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());
			prepareStatement.setString(1, depart.getDeptid());
			prepareStatement.setString(2, depart.getDeptname());
			int result = prepareStatement.executeUpdate();
			connectionManager.commitTransaction();
			return result;
		} catch (SQLException e) {
			connectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public int update(DepartDTO depart) {
		MyConnectionManager connectionManager = new MyConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("UPDATE");
			sb.append("        dept");
			sb.append("    SET");
			sb.append("        deptname = ?");
			sb.append("    WHERE");
			sb.append("        deptid = ?");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());
			prepareStatement.setString(1, depart.getDeptname());
			prepareStatement.setString(2, depart.getDeptid());
			int result = prepareStatement.executeUpdate();
			connectionManager.commitTransaction();
			return result;
		} catch (SQLException e) {
			connectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}

	}

	public ArrayList<DepartDTO> search(int setsearch, String elements) {

		MyConnectionManager connectionManager = new MyConnectionManager();

		try {
			Connection connection = connectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			if (setsearch == 0) {
				sb.append("SELECT");
				sb.append("        *");
				sb.append("    FROM");
				sb.append("        dept");
				sb.append("    WHERE");
				sb.append("        deptname LIKE ?");
			} else if (setsearch == 1) {
				sb.append("SELECT");
				sb.append("        *");
				sb.append("    FROM");
				sb.append("        dept");
				sb.append("    WHERE");
				sb.append("        deptid LIKE ?");

			}
			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());

			prepareStatement.setString(1, "%" + elements + "%");

			ResultSet rs = prepareStatement.executeQuery();

			ArrayList<DepartDTO> empList = new ArrayList<>();
			while (rs.next()) {
				String c1 = rs.getString("deptid");
				String c2 = rs.getString("deptname");

				DepartDTO DepartDTO = new DepartDTO();

				DepartDTO.setDeptid(c1);
				DepartDTO.setDeptname(c2);

				empList.add(DepartDTO);

			}
			return empList;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}
	}

}
