package servlet.change;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DepartDAO;
import dto.DepartDTO;
import dto.EmpDTO;

/**
 * Servlet implementation class EmpChangeInputGo
 */
@WebServlet("/empchangeinputgoservlet")
public class EmpChangeInputGoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// 一人の情報を更新する
		HttpSession session = request.getSession();

		EmpDTO empDTO = new EmpDTO();
		EmpDTO empone = (EmpDTO) session.getAttribute("empone");

		String empname = request.getParameter("name");
		String address = request.getParameter("address");
		String birthday = request.getParameter("birthday");
		String salary = request.getParameter("salary");
		String depart = request.getParameter("depart");

		String password = request.getParameter("password");
		String repass = request.getParameter("repassword");

		// 誕生日未入力
		if (birthday.isEmpty()) {
			session.setAttribute("passerr", "誕生日が入力されていません");
			errReturn(request, response, empname, address, birthday, salary, depart);
		}

		// 給与未入力, 給与欄に数値以外が入力されたか調べる
		if (salary.isEmpty()) {
			session.setAttribute("passerr", "給与が未入力です");
			errReturn(request, response, empname, address, birthday, salary, depart);

		} else if (!isNumber(salary)) {
			session.setAttribute("passerr", "給与は数値で入力してください");
			errReturn(request, response, empname, address, birthday, salary, depart);
		}

		// 社員名未入力
		if (empname.isEmpty()) {
			session.setAttribute("passerr", "社員名が未入力です");
			errReturn(request, response, empname, address, birthday, salary, depart);
		}
		// メールアドレス未入力
		if (address.isEmpty()) {
			session.setAttribute("passerr", "メールアドレスが未入力です");
			errReturn(request, response, empname, address, birthday, salary, depart);

		}

		// パスワード以外の入力が正常である場合データをセット
		empDTO.setEmpid(empone.getEmpid());
		empDTO.setName(empname);
		empDTO.setSalary(Integer.parseInt(salary));
		empDTO.setDepartment(depart);
		empDTO.setBirthday(Date.valueOf(birthday));
		empDTO.setPassword(password);
		empDTO.setAddress(address);

		DepartDAO departDAO = new DepartDAO();
		DepartDTO one = departDAO.getOne(empDTO.getDepartment());

		empDTO.setDepartname(one.getDeptname());
		session.setAttribute("empup", empDTO);

		// パスワードチェック
		if (password != "" && repass != "" && password.equals(repass)) {

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/Emp/Change/ChangeConfirm.jsp");
			requestDispatcher.forward(request, response);
		} else if (password == "" || repass == "") {
			session.setAttribute("passerr", "パスワードを入力してください");

		} else if (!password.equals(repass)) {
			session.setAttribute("passerr", "パスワードが正しくありません");

		} else {
			session.setAttribute("passerr", "パスワードに不正があります");

		}
		errReturn(request, response, empname, address, birthday, salary, depart);

	}

	public void errReturn(HttpServletRequest request, HttpServletResponse response, String empname, String address,
			String birthday, String salary, String depart) throws ServletException, IOException {
		HttpSession session = request.getSession();
		EmpDTO empDTO = new EmpDTO();
		EmpDTO empone = (EmpDTO) session.getAttribute("empone");
		request.setAttribute("name", empname);
		request.setAttribute("empid", empone.getEmpid());
		request.setAttribute("address", address);
		request.setAttribute("birthday", birthday);
		request.setAttribute("salary", salary);
		request.setAttribute("depart", depart);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/Emp/Change/EmpChange.jsp");
		requestDispatcher.forward(request, response);

	}

	public boolean isNumber(String number) {
		java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("^[0-9]*$");
		java.util.regex.Matcher matcher = pattern.matcher(number);
		return matcher.matches();
	}

}
