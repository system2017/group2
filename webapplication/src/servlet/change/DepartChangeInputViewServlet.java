package servlet.change;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.DepartDTO;

/**
 * Servlet implementation class DepartChangeInputViewServlet
 */
@WebServlet("/departchangeinputviewservlet")
public class DepartChangeInputViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		DepartDTO deptone = (DepartDTO) session.getAttribute("deptone");
		request.setAttribute("deptid", deptone.getDeptid());
		request.setAttribute("deptname", deptone.getDeptname());

		RequestDispatcher rd = request.getRequestDispatcher("/jsp/Depart/Change/DepartChange.jsp");
		rd.forward(request, response);
	}

}
