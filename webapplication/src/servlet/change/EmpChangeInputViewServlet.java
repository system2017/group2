package servlet.change;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DepartDAO;
import dto.DepartDTO;
import dto.EmpDTO;

/**
 * Servlet implementation class EmpChangeInputViewServlet
 */
@WebServlet("/empchangeinputviewservlet")
public class EmpChangeInputViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		EmpDTO empone = (EmpDTO) session.getAttribute("empone");
		request.setAttribute("name", empone.getName());
		request.setAttribute("empid", empone.getEmpid());
		System.out.println(empone.getEmpid());
		request.setAttribute("address", empone.getAddress());
		request.setAttribute("birthday", empone.getBirthday());
		request.setAttribute("password", empone.getPassword());
		request.setAttribute("salary", empone.getSalary());
		DepartDAO departDAO = new DepartDAO();

		ArrayList<DepartDTO> departlist = new ArrayList<DepartDTO>(departDAO.getAll());

		RequestDispatcher rd = request.getRequestDispatcher("/jsp/Emp/Change/EmpChange.jsp");
		rd.forward(request, response);
	}

}
