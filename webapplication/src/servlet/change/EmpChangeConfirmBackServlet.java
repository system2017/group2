package servlet.change;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.EmpDTO;

/**
 * Servlet implementation class EmpChangeConfirmBackServlet
 */
@WebServlet("/empchangeconfirmbackservlet")
public class EmpChangeConfirmBackServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		EmpDTO empOne = (EmpDTO) session.getAttribute("empup");

		request.setAttribute("name", empOne.getName());
		request.setAttribute("empid", empOne.getEmpid());
		request.setAttribute("address", empOne.getAddress());
		request.setAttribute("birthday", empOne.getBirthday());
		request.setAttribute("salary", empOne.getSalary());
		request.setAttribute("depart", empOne.getDepartment());

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/Emp/Change/EmpChange.jsp");
		requestDispatcher.forward(request, response);

	}

}
