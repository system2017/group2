package servlet.change;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DepartDAO;
import dto.DepartDTO;

/**
 * Servlet implementation class DepartChangeConfirmGoServlet
 */
@WebServlet("/departchangeconfirmgoservlet")
public class DepartChangeConfirmGoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		DepartDTO DepartDTO = (DepartDTO) session.getAttribute("deptup");
		DepartDAO DepartDAO = new DepartDAO();

		DepartDAO.update(DepartDTO);
		session.setAttribute("deptDTO", DepartDTO);

		response.sendRedirect("/webapplication/departlistservlet");
	}

}
