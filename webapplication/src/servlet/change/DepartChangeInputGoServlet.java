package servlet.change;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.DepartDTO;

/**
 * Servlet implementation class DepartChangeInputGoServlet
 */
@WebServlet("/departchangeinputgoservlet")
public class DepartChangeInputGoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String deptid = request.getParameter("deptid");
		String deptname = request.getParameter("deptname");

		// 一人の情報を更新する
		HttpSession session = request.getSession();

		DepartDTO departDTO = new DepartDTO();

		departDTO.setDeptid(deptid);
		departDTO.setDeptname(deptname);

		session.setAttribute("deptup", departDTO);

		RequestDispatcher requestDispatcher = request
				.getRequestDispatcher("/jsp/Depart/Change/DepartChangeConfirm.jsp");
		requestDispatcher.forward(request, response);

	}

}
