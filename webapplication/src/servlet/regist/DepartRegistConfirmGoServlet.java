package servlet.regist;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DepartDAO;
import dto.DepartDTO;

/**
 * Servlet implementation class DepartRegistConfirmGoServlet
 */
@WebServlet("/departregistconfirmgoservlet")
public class DepartRegistConfirmGoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		DepartDAO DepartDAO = new DepartDAO();
		DepartDTO Depart = new DepartDTO();

		Depart.setDeptid((String) session.getAttribute("deptid"));
		Depart.setDeptname((String) session.getAttribute("deptname"));

		DepartDAO.insert(Depart);

		response.sendRedirect("/webapplication/departlistservlet");
	}

}
