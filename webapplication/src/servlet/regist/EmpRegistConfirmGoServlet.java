package servlet.regist;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmpDAO;
import dto.EmpDTO;

/**
 * Servlet implementation class EmpRegistConfirmGoServlet
 */
@WebServlet("/empregistconfirmgoservlet")
public class EmpRegistConfirmGoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		EmpDTO empDTO = (EmpDTO) session.getAttribute("empone");
		EmpDAO empDAO = new EmpDAO();

		empDAO.insert(empDTO);
		session.setAttribute("empDTO", empDTO);

		response.sendRedirect("/webapplication/emplistservlet");

	}

}
