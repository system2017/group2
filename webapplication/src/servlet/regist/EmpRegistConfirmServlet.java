package servlet.regist;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DepartDAO;
import dao.EmpDAO;
import dto.DepartDTO;
import dto.EmpDTO;

/**
 * Servlet implementation class EmpRegistConfirmServlet
 */
@WebServlet("/empregistconfirmservlet")
public class EmpRegistConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String empid = request.getParameter("empid");
		String empname = request.getParameter("name");
		String address = request.getParameter("address");
		String birthday = request.getParameter("birthday");
		String salary = request.getParameter("salary");
		String depart = request.getParameter("departid");
		String password = request.getParameter("password");
		String repassword = request.getParameter("repassword");

		HttpSession session = request.getSession();

		EmpDTO empDTO = new EmpDTO();

		// 入力欄に空欄があるかチェック
		if (empid.isEmpty()) {
			session.setAttribute("passerr", "社員IDが入力されていません");
			errReturn(request, response, empid, empname, address, birthday, salary, depart);
		} else if (empname.isEmpty()) {
			session.setAttribute("passerr", "社員名が入力されていません");
			errReturn(request, response, empid, empname, address, birthday, salary, depart);
		} else if (address.isEmpty()) {
			session.setAttribute("passerr", "メールアドレスが入力されていません");
			errReturn(request, response, empid, empname, address, birthday, salary, depart);
		} else if (birthday.isEmpty()) {
			session.setAttribute("passerr", "誕生日が入力されていません");
			errReturn(request, response, empid, empname, address, birthday, salary, depart);
		} else if (salary.isEmpty()) {
			session.setAttribute("passerr", "給与が入力されていません");
			errReturn(request, response, empid, empname, address, birthday, salary, depart);
		}

		// 社員IDがすでに登録されているかチェック
		EmpDAO check = new EmpDAO();
		if (check.getOne(empid) != null) {
			session.setAttribute("passerr", "社員IDが重複しています");
			errReturn(request, response, empid, empname, address, birthday, salary, depart);
		}
		// 給与に数値が入力されているかチェック
		if (!isNumber(salary)) {
			session.setAttribute("passerr", "給与は数値で入力してください");
			errReturn(request, response, empid, empname, address, birthday, salary, depart);

		}

		// 入力欄にエラーがない
		empDTO.setEmpid(empid);
		empDTO.setName(empname);
		empDTO.setSalary(Integer.parseInt(salary));
		empDTO.setDepartment(depart);
		empDTO.setBirthday(Date.valueOf(birthday));
		empDTO.setPassword(password);
		empDTO.setAddress(address);

		DepartDAO departDAO = new DepartDAO();
		DepartDTO one = departDAO.getOne(empDTO.getDepartment());

		empDTO.setDepartname(one.getDeptname());
		session.setAttribute("empone", empDTO);

		if (password.equals(repassword) && password != "") {
			session.setAttribute("password", password);
			session.removeAttribute("passerr");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/Emp/Regist/EmpRegistConfirm.jsp");
			requestDispatcher.forward(request, response);

		} else {
			session.setAttribute("passerr", "パスワードが間違っています");
			errReturn(request, response, empid, empname, address, birthday, salary, depart);
		}

	}

	public void errReturn(HttpServletRequest request, HttpServletResponse response, String empid, String empname,
			String address, String birthday, String salary, String depart) throws ServletException, IOException {
		request.setAttribute("name", empname);
		request.setAttribute("empid", empid);
		request.setAttribute("address", address);
		request.setAttribute("birthday", birthday);
		request.setAttribute("salary", salary);
		request.setAttribute("depart", depart);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/Emp/Regist/EmpRegist.jsp");
		requestDispatcher.forward(request, response);
	}

	public boolean isNumber(String number) {
		java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("^[0-9]*$");
		java.util.regex.Matcher matcher = pattern.matcher(number);
		return matcher.matches();
	}
}
