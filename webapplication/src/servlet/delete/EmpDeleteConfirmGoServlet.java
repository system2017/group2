package servlet.delete;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmpDAO;
import dto.EmpDTO;

/**
 * Servlet implementation class EmpChangeConfirmGoServlet
 */
@WebServlet("/empdeleteconfirmgoservlet")
public class EmpDeleteConfirmGoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		EmpDTO empone = (EmpDTO) session.getAttribute("empone");

		EmpDAO empDAO = new EmpDAO();
		empDAO.delete(empone.getEmpid());

		// session.setAttribute("message", empone.getName() + "さんの情報を削除しました");

		response.sendRedirect("/webapplication/emplistservlet");
	}

}
