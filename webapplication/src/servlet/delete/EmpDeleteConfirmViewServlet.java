package servlet.delete;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class EmpDeleteConfirmViewServlet
 */
@WebServlet("/empdeleteconfirmviewservlet")
public class EmpDeleteConfirmViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		// HttpSession session = request.getSession();
		// EmpDTO empone = (EmpDTO) session.getAttribute("");

		RequestDispatcher rd = request.getRequestDispatcher("jsp/Emp/Delete/EmpDeleteConfirm.jsp");
		rd.forward(request, response);
	}

}
