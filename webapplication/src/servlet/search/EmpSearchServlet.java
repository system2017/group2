package servlet.search;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DepartDAO;
import dao.EmpDAO;
import dto.DepartDTO;
import dto.EmpDTO;

/**
 * Servlet implementation class EmpSearchServlet
 */
@WebServlet("/empsearchservlet")
public class EmpSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EmpSearchServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String elements = request.getParameter("elements");
		String select = request.getParameter("setsearch");
		int setsearch;
		if (select.equals("name")) {
			setsearch = 0;
		} else {
			setsearch = 1;
		}

		EmpDAO empDAO = new EmpDAO();
		ArrayList<EmpDTO> result = new ArrayList<EmpDTO>(empDAO.search(setsearch, elements));
		HttpSession session = request.getSession();
		DepartDAO departDAO = new DepartDAO();
		DepartDTO departDTO = null;
		for (EmpDTO empDTO : result) {
			departDTO = departDAO.getOne(empDTO.getDepartment());
			empDTO.setDepartname(departDTO.getDeptname());
		}

		session.setAttribute("list", result);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/Emp/Search/EmpSearch.jsp");
		requestDispatcher.forward(request, response);

	}

}
