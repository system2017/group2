package servlet.search;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DepartDAO;
import dto.DepartDTO;

/**
 * Servlet implementation class DepartSearchServlet
 */
@WebServlet("/departsearchservlet")
public class DepartSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String elements = request.getParameter("elements");
		String select = request.getParameter("setsearch");
		int setsearch;
		if (select.equals("name")) {
			setsearch = 0;
		} else {
			setsearch = 1;
		}

		DepartDAO DepartDAO = new DepartDAO();
		ArrayList<DepartDTO> result = new ArrayList<DepartDTO>(DepartDAO.search(setsearch, elements));
		HttpSession session = request.getSession();

		session.setAttribute("list", result);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/Depart/Search/DepartSearch.jsp");
		requestDispatcher.forward(request, response);
	}

}
