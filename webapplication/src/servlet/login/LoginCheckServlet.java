package servlet.login;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmpDAO;
import dto.EmpDTO;

/**
 * Servlet implementation class LoginCheckServlet
 */
@WebServlet("/logincheckservlet")
public class LoginCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String loginid = request.getParameter("loginid"); // ログインidの変数
		String loginpass = request.getParameter("loginpass"); // ログインパスワードの変数

		if (loginid.isEmpty()) {
			request.setAttribute("err", "ログイン情報が不正です");
			request.setAttribute("loginid", loginid);
			RequestDispatcher rd = request.getRequestDispatcher("/loginviewservlet");
			rd.forward(request, response);
		} else if (loginpass.isEmpty()) {
			request.setAttribute("err", "ログイン情報が不正です");
			request.setAttribute("loginid", loginid);
			RequestDispatcher rd = request.getRequestDispatcher("/loginviewservlet");
			rd.forward(request, response);
		}
		EmpDAO empDAO = new EmpDAO(); // DAO出来次第左辺書く

		EmpDTO empone = null;
		empone = empDAO.getOne(loginid);

		if (empone != null) {
			if (loginpass.equals(empone.getPassword())) {
				HttpSession session = request.getSession();
				session.setAttribute("login", empone);
				response.sendRedirect("/webapplication/menu");
			} else {
				request.setAttribute("err", "ログイン情報が不正です");
				request.setAttribute("loginid", loginid);
				RequestDispatcher rd = request.getRequestDispatcher("/loginviewservlet");
				rd.forward(request, response);
			}
		} else {
			request.setAttribute("err", "ログイン情報が不正です");
			request.setAttribute("loginid", loginid);
			RequestDispatcher rd = request.getRequestDispatcher("/loginviewservlet");
			rd.forward(request, response);

		}

	}

}
