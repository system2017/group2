package servlet.detail;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DepartDAO;
import dao.EmpDAO;
import dto.DepartDTO;
import dto.EmpDTO;

/**
 * Servlet implementation class EmpDetailViewServlet
 */
@WebServlet("/empdetailviewservlet")
public class EmpDetailViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		String emplistid = request.getParameter("empid");

		EmpDAO empDAO = new EmpDAO();
		EmpDTO empone = empDAO.getOne(emplistid);

		DepartDAO departDAO = new DepartDAO();
		DepartDTO departone = departDAO.getOne(empone.getDepartment());

		empone.setDepartname(departone.getDeptname());
		session.setAttribute("empone", empone); // 詳細で表示した一人分の社員情報をセッションに保存する(変更削除に使用するため)

		RequestDispatcher rd = request.getRequestDispatcher("/jsp/Emp/Detail/EmpDetail.jsp");
		rd.forward(request, response);
	}

}
