package servlet.detail;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DepartDAO;
import dao.EmpDAO;
import dto.DepartDTO;
import dto.EmpDTO;

/**
 * Servlet implementation class DepartDetailViewServlet
 */
@WebServlet("/departdetailviewservlet")
public class DepartDetailViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String deptlistid = request.getParameter("deptid");
		DepartDAO departDAO = new DepartDAO();
		DepartDTO deptone = departDAO.getOne(deptlistid);

		EmpDAO empDAO = new EmpDAO();
		ArrayList<EmpDTO> empall = empDAO.getAll();
		ArrayList<EmpDTO> emplist = new ArrayList<EmpDTO>();
		for (EmpDTO empDTO : empall) {
			if (empDTO.getDepartment().equals(deptlistid)) {
				emplist.add(empDTO);
			}
		}

		session.setAttribute("emppartlist", emplist);
		session.setAttribute("deptone", deptone);

		RequestDispatcher rd = request.getRequestDispatcher("/jsp/Depart/Detail/DepartDetail.jsp");
		rd.forward(request, response);
	}

}
