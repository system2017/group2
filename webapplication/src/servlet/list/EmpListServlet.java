package servlet.list;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DepartDAO;
import dao.EmpDAO;
import dto.DepartDTO;
import dto.EmpDTO;

/**
 * Servlet implementation class EmpListServlet
 */
@WebServlet("/emplistservlet")
public class EmpListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		EmpDAO empDAO = new EmpDAO();
		DepartDAO departDAO = new DepartDAO();
		DepartDTO departDTO = null;
		ArrayList<EmpDTO> emplist = new ArrayList<EmpDTO>(empDAO.getAll());
		ArrayList<DepartDTO> departlist = new ArrayList<DepartDTO>(departDAO.getAll());
		session.setAttribute("emplist", emplist);
		session.setAttribute("departlist", departlist);
		for (EmpDTO empDTO : emplist) {
			departDTO = departDAO.getOne(empDTO.getDepartment());
			empDTO.setDepartname(departDTO.getDeptname());
		}

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/Emp/List/EmpList.jsp");
		requestDispatcher.forward(request, response);

		session.removeAttribute("passerr");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
