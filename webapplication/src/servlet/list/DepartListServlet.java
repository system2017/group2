package servlet.list;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DepartDAO;
import dao.EmpDAO;
import dto.DepartDTO;
import dto.EmpDTO;

/**
 * Servlet implementation class DepartListServlet
 */
@WebServlet("/departlistservlet")
public class DepartListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		// 各セッションの開放
		session.removeAttribute("emplist");
		session.removeAttribute("Departlist");
		session.removeAttribute("deptid");
		session.removeAttribute("deptname");
		session.removeAttribute("deptone");
		EmpDAO empDAO = new EmpDAO();
		DepartDAO departDAO = new DepartDAO();

		ArrayList<EmpDTO> emplist = new ArrayList<EmpDTO>(empDAO.getAll());
		ArrayList<DepartDTO> departlist = new ArrayList<DepartDTO>(departDAO.getAll());

		session.setAttribute("emplist", emplist);
		session.setAttribute("departlist", departlist);

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/Depart/List/DepartList.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
