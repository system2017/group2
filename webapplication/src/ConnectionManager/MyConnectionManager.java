package ConnectionManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyConnectionManager {

	static {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e1) {
			throw new RuntimeException(e1);
		}
	}

	Connection connection;

	public Connection getConnection() {
		try {
			connection = DriverManager.getConnection("jdbc:postgresql:webapplication", "webapplication",
					"webapplication");
			System.out.println("DB 接続完了");
			connection.setAutoCommit(false);
			return connection;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void closeConnection() {
		if (connection != null) {
			try {
				if (connection != null) {
					connection.close();
				}
				System.out.println("DB 切断完了");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void commitTransaction() {
		try {
			connection.commit();
			System.out.println("コミット完了");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void rollbackTransaction() {
		try {
			if (connection != null) {
				connection.rollback();
				System.out.println("ロールバック完了");
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

}
