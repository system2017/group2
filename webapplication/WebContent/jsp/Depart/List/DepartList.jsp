<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/jsp/Menu/DepartMenuBar.jsp"%>


	<div class="search">
		<div class="login-screen">
			<table>
				<tr>
					<th>
						<form action="/webapplication/departsearchservlet" method="post">
							<input type="search" name="elements" placeholder="Search">
					</th>

					<td width="500">
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
									<div class="select-wrap select-primary">
										<select style="width: 200" name="setsearch">
											<option value="name">部署名</option>
											<option value="id">ID</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</td>

					<th><input class="mini" type="submit" value="検索"></th>
					</form>

				</tr>
			</table>

		</div>
	</div>

	<div class="list">
		<div class="login-screen">
			<div class="app-title">
				<h1>部署一覧</h1>
			</div>
			<table border="1" width="500" cellspacing="0" cellpadding="5">
				<tr>
					<th>部署ID</th>
					<th>部署名</th>
					<th>詳細</th>
				</tr>
				<c:forEach items="${departlist}" var="e">
					<tr>
						<th>${e.deptid}</th>
						<th>${e.deptname}</th>
						<th width="100"><form
								action="/webapplication/departdetailviewservlet" method="post">
								<input class="mini" type="submit" value="詳細"><input
									type="hidden" name="deptid" value="${e.deptid }"></input>
							</form></th>
					</tr>
					<tr>
				</c:forEach>
			</table>
		</div>
	</div>

</body>
</html>