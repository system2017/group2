<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>部署詳細</h1>

	<table border="1">
		<tr>
			<th>部署ID:</th>
			<th>${deptone.deptid }</th>
		</tr>
		<tr>
			<th>部署名:</th>
			<th>${deptone.deptname }</th>
		</tr>
	</table>
<br>
<h1>所属している社員名</h1>
	<table border="1">
		<c:forEach items="${emppartlist}" var="e">
			<tr>
				<th>社員名ID:</th>
				<th>${e.empid }</th>
			</tr>
			<tr>
				<th>社員名:</th>
				<th>${e.name }</th>
			</tr>
		</c:forEach>
	</table>
	<form action="/webapplication/departlistservlet" method="post">
		<input type="submit" value="一覧に戻る">
	</form>
	<form action="/webapplication/departchangeinputviewservlet"
		method="post">
		<input type="submit" value="部署情報変更">
	</form>


</body>
</html>