<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>部署情報変更</h1>

	<h2>変更する部署情報を入力してください。</h2>
	<form action="/webapplication/departchangeinputgoservlet" method="post">
		<table border="1">
			<tr>
				<th>部署ID:</th>
				<th><c:out value="${deptid }"></c:out></th>
			</tr>
			<tr>
				<th>部署名:</th>
				<th><input name="deptname" value="${deptname }"></th>
			</tr>

		</table>
		<input type="submit" value="確認">
	</form>
	<form action="/webapplication/departdetailviewservlet"
		method="post">
		<input type="submit" value="戻る">
		<input type="hidden" name="deptid" value="${deptid }">
	</form>

</body>
</html>