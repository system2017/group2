<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/webapplication/CSS/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="login">
		<div class="login-screen">
			<div class="app-title">
				<h1>社員管理システム</h1>
			</div>

			<div class="text">
				${err }
			</div>

			<div class="login-form">
				<div class="control-group">
					<form action="/webapplication/logincheckservlet" method="post">
						<input name="loginid" class="login-field" value="${loginid }"
							placeholder="ID" id="login-name"> <label
							class="login-field-icon fui-user" for="login-name"></label>
				</div>
				<div class="control-group">
					<input type="password" name="loginpass" class="login-field"
						value="" placeholder="password" id="login-pass"> <label
						class="login-field-icon fui-lock" for="login-pass"></label>
				</div>
				<input class="btn btn-primary btn-large btn-block" type="submit"
					value="login">
				</form>
			</div>
		</div>
	</div>
</body>
</html>