<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/jsp/Menu/EmpMenuBar.jsp"%>

	<div class="list">
		<div class="login-screen">
			<div class="app-title">
				<h1>社員登録</h1>
				<h3>登録する社員情報を入力してください。</h3>
			</div>


			${passerr }
			<form action="/webapplication/empregistconfirmservlet" method="post">
				<table align="center" border="1">
					<tr>
						<th>社員名ID:</th>
						<th><input name="empid" value="${empid }"></th>
					</tr>
					<tr>
						<th>社員名:</th>
						<th><input name="name" value="${name }"></th>
					</tr>
					<tr>
						<th>メールアドレス:</th>
						<th><input name="address" value="${address }"></th>
					</tr>
					<tr>
						<th>生年月日:</th>
						<th><input name="birthday" type="date" value="${birthday }"></th>
					</tr>
					<tr>
						<th>給与:</th>
						<th><input name="salary" value="${salary }"></th>
					</tr>
					<tr>
						<th>部署:</th>
						<th>
							<div class="col-xs-12">
								<div class="form-group">
									<div class="select-wrap select-primary">
										<select name="departid">
											<c:forEach var="e" items="${departlist }" varStatus="i">
												<option value="${e.deptid }">${e.deptname }</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
							</div>
						</th>
					</tr>
					<tr>
						<th>パスワード:</th>
						<th><input name="password"></th>
					</tr>
					<tr>
						<th>パスワード(確認):</th>
						<th><input name="repassword"></th>
					</tr>
				</table>


				<table align="center" cellpadding="20">
					<tr>
						<th><input class="mini" type="submit" value="確認">
							</form></th>
						<th>
							<form action="/webapplication/emplistservlet" method="post">
								<input class="mini" type="submit" value="一覧に戻る">
							</form>
						</th>
					</tr>
				</table></body>
</html>