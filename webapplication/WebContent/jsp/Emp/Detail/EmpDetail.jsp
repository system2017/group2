<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css"
	href="/webapplication/CSS/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<%@ include file="/jsp/Menu/EmpMenuBar.jsp"%>

	<div class="detail">
		<div class="login-screen">
			<div class="app-title">
				<h1>社員詳細</h1>
			</div>

			<table align="center" border="1" cellspacing="0" cellpadding="5">
				<tr>
					<th>社員名ID:</th>
					<th>${empone.empid }</th>
				</tr>
				<tr>
					<th>社員名:</th>
					<th>${empone.name }</th>
				</tr>
				<tr>
					<th>メールアドレス:</th>
					<th>${empone.address }</th>
				</tr>
				<tr>
					<th>生年月日:</th>
					<th>${empone.birthday }</th>
				</tr>
				<tr>
					<th>給与:</th>
					<th>${empone.salary }</th>
				</tr>
				<tr>
					<th>部署:</th>
					<th>${empone.departname }</th>
				</tr>

			</table>

			<table align="center" cellpadding="10">
				<tr>
					<th>
						<form action="/webapplication/emplistservlet" method="post">
							<input class="mini" type="submit" value="一覧に戻る">
						</form>
					</th>
					<th>
						<form action="/webapplication/empchangeinputviewservlet"
							method="post">
							<input class="mini" type="submit" value="社員情報変更">
						</form>
					</th>
					<th>
						<form action="/webapplication/empdeleteconfirmviewservlet"
							method="post">
							<input class="mini" type="submit" value="社員削除">
						</form>
					</th>
				</tr>
			</table>

		</div>
	</div>

</body>
</html>