<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html >
<html>
<head>
<link rel="stylesheet" type="text/css" href="/webapplication/CSS/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/jsp/Menu/EmpMenuBar.jsp"%>

	<div class="search">
		<div class="login-screen">
			<table>
				<tr>
					<th>
						<form action="/webapplication/empsearchservlet" method="post">
							<input type="search" name="elements" placeholder="Search">
					</th>

					<td width="500">
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
									<div class="select-wrap select-primary">
										<select style="width: 200" name="setsearch">
											<option value="name">社員名</option>
											<option value="id">ID</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</td>

					<th><input class="mini" type="submit" value="検索"></th>
					</form>

				</tr>
			</table>

		</div>
	</div>


	<div class="list">
		<div class="login-screen">
		<div class="app-title">
				<h1>社員一覧</h1>
			</div>
			<table border="1" width="500" cellspacing="0" cellpadding="5">
			<tr><th>社員ID</th><th>社員名</th><th>部署名</th><th>詳細</th></tr>
				<c:forEach items="${emplist}" var="e">
					<tr>
						<th>${e.empid}</th>
						<th>${e.name}</th>
						<th>${e.departname}</th>

						<th width="100"><form action="/webapplication/empdetailviewservlet"
								method="post">
								<input class="mini" type="submit" value="詳細"> <input
									type="hidden" name="empid" value="${e.empid}"></input>
							</form></th>
					</tr>
					<tr>
				</c:forEach>
			</table>
		</div>
	</div>

</body>
</html>