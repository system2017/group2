<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<%@ include file="/jsp/Menu/EmpMenuBar.jsp"%>

	<div class="list">
		<div class="login-screen">
			<div class="app-title">
				<h1>社員削除</h1>
				<h3>この社員を削除しますか？</h3>
			</div>

	<table align="center" border="1">
		<tr>
			<th>社員名ID:</th>
			<th>${empone.empid}</th>
		</tr>
		<tr>
			<th>社員名:</th>
			<th>${empone.name}</th>
		</tr>
		<tr>
			<th>メールアドレス:</th>
			<th>${empone.address}</th>
		</tr>
		<tr>
			<th>生年月日:</th>
			<th>${empone.birthday}</th>
		</tr>
		<tr>
			<th>給与:</th>
			<th>${empone.salary}</th>
		</tr>
		<tr>
			<th>部署:</th>
			<th>${empone.departname}</th>
		</tr>

	</table><table align="center" cellpadding="20">
					<tr>
						<th>
	<form action="/webapplication/empdetailviewservlet" method="post">
		<input class="mini" type="submit" value="詳細に戻る">
		<input type="hidden" name="empid" value="${empone.empid }"></input>
	</form></th>
						<th>
	<form action="/webapplication/empdeleteconfirmgoservlet" method="post">
		<input class="mini" type="submit" value="削除">
	</form></th>
					</tr>
				</table>



</body>
</html>