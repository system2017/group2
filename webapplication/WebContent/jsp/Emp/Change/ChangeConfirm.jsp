<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/jsp/Menu/EmpMenuBar.jsp"%>

<div class="list">
		<div class="login-screen">
			<div class="app-title">
				<h1>社員情報変更</h1>
				<h3>下記の情報で変更しますか？</h3>
			</div>

	<form action="/webapplication/empchangeconfirmgoservlet" method="post">
		<table align="center" border="1">
			<tr>
				<th>社員名ID:</th>
				<td>${empup.empid }</td>
			</tr>
			<tr>
				<th>社員名:</th>
				<td>${empup.name }</td>
			</tr>
			<tr>
				<th>メールアドレス:</th>
				<td>${empup.address }</td>
			</tr>
			<tr>
				<th>生年月日:</th>
				<td>${empup.birthday }</td>
			</tr>
			<tr>
				<th>給与:</th>
				<td>${empup.salary }</td>
			</tr>
			<tr>
				<th>部署</th>
				<td><c:out value="${empup.departname }"></c:out> </td>
			</tr>

		</table>
		<table align="center" cellpadding="20">
					<tr>
						<th>
		<input class="mini" type="submit" value="確認">
	</form></th>
						<th>
	<form action=/webapplication/empchangeconfirmbackservlet method="post" >
		<input class="mini" type="submit" value="戻る">
	</form></th>
					</tr>
				</table>
</body>
</html>