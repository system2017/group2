<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="/jsp/Menu/EmpMenuBar.jsp"%>
	<div class="list">
		<div class="login-screen">
			<div class="app-title">
				<h1>社員情報変更</h1>
				<h3>変更する社員情報を入力してください。</h3>
			</div>

			${passerr }

			<form action="/webapplication/empchangeinputgoservlet" method="post">
				<table align="center" border="1">
					<tr>
						<th>社員名ID:</th>
						<th><c:out value="${empid }"></c:out></th>


					</tr>
					<tr>
						<th>社員名:</th>
						<td><input name="name" value="${name }"></td>
					</tr>
					<tr>
						<th>メールアドレス:</th>
						<td><input name="address" value="${address }"></td>
					</tr>
					<tr>
						<th>生年月日:</th>
						<td><input name="birthday" type="date" value="${birthday }"></td>
					</tr>
					<tr>
						<th>給与:</th>
						<td><input name="salary" value="${salary }"></td>
					</tr>
					<tr>
						<th>部署:</th>
						<td>
							<div class="col-xs-12">
								<div class="form-group">
									<div class="select-wrap select-primary">
										<select name="depart">
											<c:forEach var="e" items="${departlist }" varStatus="i">
												<c:if test="${depart ==e.deptid }">
													<option value="${e.deptid }" selected="selected">${e.deptname }</option>
												</c:if>
												<c:if test="${depart !=e.deptid }">
													<option value="${e.deptid }">${e.deptname }</option>
												</c:if>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th>パスワード:</th>
						<td><input name="password" value=""></td>
					</tr>
					<tr>
						<th>パスワード(確認):</th>
						<td><input name="repassword"></td>
					</tr>
				</table>

				<table align="center" cellpadding="20">
					<tr>
						<th><input class="mini" type="submit" value="確認">
			</form>
			</th>
			<th>
				<form action="/webapplication/emplistservlet" method="post">
					<input class="mini" type="submit" value="戻る">
				</form>
			</th>
			</tr>
			</table>
</body>
</html>